#Reescribe el programa de caliﬁcaciones del capítulo anterior
#usando una función llamada calcula_calificacion, que reciba una
#puntuación como parámetro y devuelva una caliﬁcación como cadena.
#Author: Wilmer Cruz
#Email: wilmer.cruz@unl.edu.ec

puntuación = float (input("Ingrese la Calificación: "))
def calcaula_calificación(puntuación):
    try:
        if puntuación >= 0 and puntuación <= 1.0:
            if puntuación >=0.9:
                    print("sobresaliente")
            elif puntuación >0.8:
                    print ("Notable")
            elif puntuación >=0.7:
                    print ("Bien")
            elif puntuación >=0.6:
                    print("Suficiente")
            elif puntuación <0.6:
                print("Insuficiente")
        else:
            print("Fuera de rango")
    except:
        print ("puntuación incorrecta")

calcaula_calificación(puntuación)